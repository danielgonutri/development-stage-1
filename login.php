<?php
/**
* Template Name: Login
*/
?>

<?php
// IF USER IS ALREADY LOGGED -> REDIRECT HIM TO HOME PAGE
if (is_user_logged_in()) : 	
	wp_redirect( home_url() ); exit;
else : 
?>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<!-- MAKE IT RESPONSIVE -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php // GET FAVICONS
		woffice_favicons();
		?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/flexie.min.js"></script>
		<![endif]-->
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
	
		<div id="page-wrapper">
			<div id="content-container">
	
				<!-- START CONTENT -->
				<section id="woffice-login">
				
					<div id="woffice-login-left">
					</div>
					
					<div id="woffice-login-right">
						<!-- LOGO & DESCRIPTION -->
						<header>
							<a href="<?php echo home_url(); ?>" id="login-logo"></a>
							
							<?php // THE ERRORS
							$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
							if ( $login === "failed" ) {
								echo'<div class="infobox fa-meh-o" style="background-color: #DA0015;">';
									echo'<span class="infobox-head"><i class="fa fa-exclamation-triangle"></i> '. __('ERROR:','woffice').'</span>';
									echo'<p>'. __('Invalid username and/or password.','woffice') .'</p>';
								echo'</div>';
							} elseif ( $login === "empty" ) {
								echo'<div class="infobox fa-meh-o" style="background-color: #DA0015;">';
									echo'<span class="infobox-head"><i class="fa fa-exclamation-triangle"></i> '. __('ERROR:','woffice').'</span>';
									echo'<p>'. __('Username and/or Password is empty.','woffice') .'</p>';
								echo'</div>';
							} elseif ( $login === "false" ) {
								echo'<div class="infobox fa-meh-o" style="background-color: #DA0015;">';
									echo'<span class="infobox-head"><i class="fa fa-exclamation-triangle"></i> '. __('ERROR:','woffice').'</span>';
									echo'<p>'. __('You are logged out.','woffice') .'</p>';
								echo'</div>';
							}
							?>
							
							<?php // GET & DISPLAY LOGIN TEXT 
							$login_text = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('login_text') : ''; 
							if (!empty($login_text)):
								echo"<p>".$login_text."</p>";
							endif; ?>
							
						</header>
						
						<!-- FORM -->
						<?php
						$login_args = array(
							'redirect' => home_url(), 
							'id_username' => 'user',
							'id_password' => 'pass',
						);?>
						<?php wp_login_form( $login_args ); ?>
						
						<?php // LOST PASSWORD LINK 
						$login_rest_password = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('login_rest_password') : ''; 
						if ($login_rest_password == "yep"){
							echo '<a href="'. wp_lostpassword_url() .'" class="password-lost">'. __('Lost Password','woffice') .'</a>';	
						} ?>
						
						<?php 
						/* REGISTER FORM */
						if (get_option('users_can_register') == '1'){
							$register_message = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('register_message') : ''; 
							echo '<div id="register-wrapper">';
							echo '<p>'.$register_message.'</p>';
							echo '<a href="#register-form" class="btn btn-default" id="register-trigger"><i class="fa fa-sign-in"></i> Kostenlos Registrieren</a>';
							echo '</div>';
							echo '<div id="register-loader" class="intern-padding woffice-loader"><i class="fa fa-spinner"></i></div>';
							echo do_shortcode('[woffice_registration_form]');
							echo '<div id="goback-trigger"><a href="#loginform" class="btn btn-default"><i class="fa fa-arrow-left"></i> Zurück</a></div>';
						}
							
						?>
						
						
						<?php // DISPLAY WORDPRESS LINK ? 
						$login_wordpress = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('login_wordpress') : ''; 
						if ($login_wordpress == "yep"):
						?>
							<footer>
								<p>
									<?php _e("Proudly powered by","woffice"); ?> 
									<a href="https://wordpress.org/" target="_blank">
										<img src="<?php echo home_url(); ?>/wp-admin/images/wordpress-logo.svg" alt="wordpress logo">
									</a>
								</p>
							</footer>
						<?php endif; ?>
					</div>
					
				</section>
				<!-- END CONTENT -->
				
			</div>
		</div>
		
		<!-- JAVSCRIPTS BELOW AND FILES LOADED BY WORDPRESS -->
		
		<script type="text/javascript">
			if (jQuery('#success-register').length  > 0) {
				jQuery('#register-loader').slideDown();
				jQuery("#register-form, #goback-trigger").hide();
		    	function show_login() {
			    	jQuery("#loginform, #register-wrapper,a.password-lost").show();
					jQuery('#register-loader').slideUp();
				}
				setTimeout(show_login, 1000);
			}
			jQuery("#register-loader, #register-form, #goback-trigger").hide();
			jQuery("#register-trigger").on('click', function(){
		    	jQuery('#register-loader').slideDown();
				jQuery("#loginform, #register-wrapper,a.password-lost").hide();
		    	function show_register() {
			    	jQuery("#register-form, #goback-trigger").show();
					jQuery('#register-loader').slideUp();
				}
				setTimeout(show_register, 1000);
			});
			jQuery("#goback-trigger a").on('click', function(){
		    	jQuery('#register-loader').slideDown();
				jQuery("#register-form, #goback-trigger").hide();
		    	function show_login() {
			    	jQuery("#loginform, #register-wrapper,a.password-lost").show();
					jQuery('#register-loader').slideUp();
				}
				setTimeout(show_login, 1000);
			});
			var hash = location.hash.replace('#', '');
			if (hash == 'register-form') {
				if (jQuery('#success-register').length  == 0) {
					jQuery('#register-loader').slideDown();
					jQuery("#loginform, #register-wrapper,a.password-lost").hide();
			    	function show_register() {
				    	jQuery("#register-form").show();
						jQuery('#register-loader').slideUp();
					}
					setTimeout(show_register, 1000);
				}
			}
		</script>
		
		<?php wp_footer(); ?>
	</body>
</html>
<?php endif; ?>